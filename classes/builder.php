<?php
abstract class SectionBuilder
{
    protected abstract function Build($data); //Returns valid HTML string

    public final function BuildHTML($data)
    {
        $attr = "";

        if (isset($data->cssId))
        {
            $attr .= Attr("id", $data->cssId);
        }

        if (isset($data->cssClass))
        {
            $attr .= Attr("class", $data->cssClass);
        }

        $css = "";

        if (isset($data->backgroundColor))
        {
            $css .= Css("background-color", $data->backgroundColor);
        }

        if (isset($data->textColor))
        {
            $css .= Css("color", $data->textColor);
        }

        if (isset($data->font))
        {
            $css .= Css("font-family", $data->font);
        }

        if (isset($data->minHeight))
        {
            $css .= Css("min-height", $data->minHeight);
        }

        $attr .= Attr("style", $css);

        $html = $this->Build($data);

        if (isset($data->href))
        {
            $html = HTML("a", $html, Attr("href", $data->href));
        }

        return HTML("section", $html, $attr);
    }
}

abstract class ContentBuilder
{
    protected abstract function Build($data); //Returns valid HTML string

    public final function BuildHTML($data)
    {
        $attr = "";

        if (isset($data->cssId))
        {
            $attr .= Attr("id", $data->cssId);
        }

        if (isset($data->cssClass))
        {
            $attr .= Attr("class", $data->cssClass);
        }

        $css = "";

        if (isset($data->backgroundColor))
        {
            $css .= Css("background-color", $data->backgroundColor);
        }

        if (isset($data->textColor))
        {
            $css .= Css("color", $data->textColor);
        }

        if (isset($data->font))
        {
            $css .= Css("font-family", $data->font);
        }

        if (isset($data->minHeight))
        {
            $css .= Css("min-height", $data->minHeight);
        }

        $attr .= Attr("style", $css);

        $html = $this->Build($data);

        if (isset($data->href))
        {
            $html = HTML("a", $html, Attr("href", $data->href));
        }

        return HTML("div", $html, $attr);
    }
}