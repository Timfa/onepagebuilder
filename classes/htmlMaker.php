<?php
function HTML($elementType, $content, $attributes = "")
{
	$result = "<" . $elementType;
	
	$result .= " " . $attributes;
		
	$result .= ">" . $content . "</" . $elementType . ">";
	
	return $result;
}

function Attr($type, $content)
{
	$result = $type . "='";
	
	$result .= $content;
		
	$result .= "'";
	
	return $result;
}

function Css($type, $content)
{
	$result = $type . ":";
	
	$result .= $content;
		
	$result .= ";";
	
	return $result;
}

function BR()
{
	return "<br/>";
}