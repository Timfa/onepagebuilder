<?php

$builders = [];

function RegisterType($type, $builder)
{
    global $builders;

    $newBuilder = new stdClass();

    $newBuilder->type = $type;
    $newBuilder->builder = $builder;

    array_push($builders, $newBuilder);
}

function GetBuilder($type)
{
    global $builders;

    for ($i = 0; $i < count($builders); $i++)
    {
        if ($builders[$i]->type == $type)
        {
            return $builders[$i]->builder;
        }
    }

    throw new Exception('No builder with type "' . $type . '" found.');
}