<?php

class ContactFormContentBuilder extends ContentBuilder
{
    public function Build($data)
    {
        $html = "Message Title: " . HTML("input", "", Attr("type", "text") . Attr("placeholder", "Message Title") . Attr("name", "title") . Attr("id", "contact-title"));

        $html .= BR();

        $html .= "Your Email: " . HTML("input", "", Attr("type", "email") . Attr("placeholder", "your@email.com") . Attr("name", "email") . Attr("id", "contact-email"));

        $html .= BR();

        $html .= "Your Message: " . BR() . HTML("textarea", "", Attr("name", "message") . Attr("id", "contact-message"));

        $html .= BR();

        $html .= HTML("input", "", Attr("type", "submit") . Attr("value", "Send") . Attr("name", "submit") . Attr("id", "contact-submit"));

        return HTML("form", $html);
    }
}

RegisterType('content/contactForm', new ContactFormContentBuilder());