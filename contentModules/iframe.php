<?php

class IframeContentBuilder extends ContentBuilder
{
    public function Build($data)
    {
        return '<center><iframe title="iframe" type="text/html" width="' . $data->width . '" height="' . $data->height . '" src="' . $data->src . '" frameborder="0" allowFullScreen></iframe></center>';
    }
}

RegisterType('content/iframe', new IframeContentBuilder());