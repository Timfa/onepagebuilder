<?php

class ImageContentBuilder extends ContentBuilder
{
    public function Build($data)
    {
        $html = HTML("img","", Attr("src", $data->src));

        if (isset($data->text))
        {
            $html .= BR();

            $html .= HTML("text", $data->text);
        }

        return HTML("center", $html);
    }
}

RegisterType('content/image', new ImageContentBuilder());