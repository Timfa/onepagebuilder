<?php

class ListContentBuilder extends ContentBuilder
{
    public function Build($data)
    {
        $html = HTML("h2", $data->title);

        $list = "";

        for ($i = 0; $i < count($data->listItems); $i++)
        {
            if (!isset($data->listItems[$i]->href))
            {
                $list .= HTML("li", $data->listItems[$i]->text);
            }
            else
            {
                $list .= HTML("a", HTML("li", $data->listItems[$i]->text), Attr("href", $data->listItems[$i]->href));
            }
        }
        
        $html .= HTML("ul", $list);

        return $html;
    }
}

RegisterType('content/list', new ListContentBuilder());