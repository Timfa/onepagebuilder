<?php

class TextContentBuilder extends ContentBuilder
{
    public function Build($data)
    {
        $html = HTML("h2", $data->title);

        $html .= BR();

        $html .= HTML("text", $data->text);

        $elementType = "center";

        if (isset($data->center))
        {
            $elementType = $data->center == false? "div" : "center";
        }

        return HTML($elementType, $html, Attr("style", Css("align-items", "center"))); // Todo: make vertical center work
    }
}

RegisterType('content/text', new TextContentBuilder());