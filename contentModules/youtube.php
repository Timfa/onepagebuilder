<?php

class YoutubeContentBuilder extends ContentBuilder
{
    public function Build($data)
    {
        return '<center><iframe title="YouTube video player" class="youtube-player" type="text/html" width="' . $data->width . '" height="' . $data->height . '" src="http://www.youtube.com/embed/' . $data->videoURL . '" frameborder="0" allowFullScreen></iframe></center>';
    }
}

RegisterType('content/youtube', new YoutubeContentBuilder());