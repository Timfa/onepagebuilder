<?php

require_once 'classes/htmlMaker.php';
require_once 'classes/builder.php';
require_once 'classes/typeRegisterer.php';

// Include all section modules
foreach (glob("sectionModules/*.php") as $filename)
{
    require_once $filename;
}


// Include all content modules
foreach (glob("contentModules/*.php") as $filename)
{
    require_once $filename;
}