<?php

require_once 'includes.php';

$json = file_get_contents("structure.json");

$structure = json_decode($json);

echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
echo "<html>";
echo "<head>";
echo '<link rel="stylesheet" href="css/strip.css">';
for ($i = 0; $i < count($structure->stylesheets); $i++)
{
    echo '<link rel="stylesheet" href="' . $structure->stylesheets[$i] . '.css">';
}
echo "</head>";
echo '<body style="margin-top: ' . $structure->header->height . ';">';

$headerItems = HTML("a", HTML("img", "", Attr("id", "headerLogo") . Attr("src", $structure->header->icon->src)), Attr("href", "."));

for ($i = count($structure->header->items) - 1; $i >= 0; $i--)
{
    $classAndId = "";

    if (isset($structure->header->items[$i]->cssId))
        $classAndId .= Attr("id", $structure->header->items[$i]->cssId);

    if (isset($structure->header->items[$i]->cssClass))
        $classAndId .= Attr("class", $structure->header->items[$i]->cssClass);

    $headerItems .= HTML("div", $structure->header->items[$i]->text . " ", $classAndId . Attr("style", Css("padding-right", "10px") . Css("padding-top", "15px") . Css("float", "right")));
}

$classAndId = "";

if (isset($structure->header->cssId))
    $classAndId .= Attr("id", $structure->header->cssId);

if (isset($structure->header->cssClass))
    $classAndId .= Attr("class", $structure->header->cssClass);

echo HTML("section", $headerItems, $classAndId . Attr("style", Css("background-color", $structure->header->backgroundColor) . Css("color", $structure->header->textColor) . Css("font-family", $structure->header->font) . Css("position", "fixed") . Css("top", "0px") . Css("width", "100%") . Css("height", $structure->header->height)));

for ($i = 0; $i < count($structure->sections); $i++)
{
    $section = $structure->sections[$i];

    $builder = GetBuilder("section/" . $section->type);

    echo $builder->BuildHTML($section);
}

for ($i = 0; $i < count($structure->scripts); $i++)
{
    echo '<script src="' . $structure->scripts[$i] . '"></script>';
}

echo "</body>";
echo "</html>";