<script>
jQuery( function() {

var pe_nav = jQuery( 'nav' ),
$window = jQuery( window );

function pe_check_scroll_position() {

if ( $window.scrollTop() == 0 ) {
pe_nav.addClass( 'topped' );
} else {
pe_nav.removeClass( 'topped' );
}

}
pe_check_scroll_position();

$window.scroll( pe_check_scroll_position );
});
</script>