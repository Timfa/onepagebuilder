<?php

class FourWaySectionBuilder extends SectionBuilder
{
    protected function Build($data)
    {
        $leftType = $data->firstContent->type;

        $htmlContentL = GetBuilder("content/" . $leftType)->BuildHTML($data->firstContent);
        
        $left2Type = $data->secondContent->type;

        $htmlContentL2 = GetBuilder("content/" . $left2Type)->BuildHTML($data->secondContent);

        $rightType = $data->thirdContent->type;

        $htmlContentR = GetBuilder("content/" . $rightType)->BuildHTML($data->thirdContent);

        $right2Type = $data->fourthContent->type;

        $htmlContentR2 = GetBuilder("content/" . $right2Type)->BuildHTML($data->fourthContent);

        $lDiv = HTML("div", $htmlContentL, Attr("style", Css("display", "inline-block") . Css("width", "25%") . Css("vertical-align", "top")));
        $l2Div = HTML("div", $htmlContentL2, Attr("style", Css("display", "inline-block") . Css("width", "25%") . Css("vertical-align", "top")));
        $rDiv = HTML("div", $htmlContentR, Attr("style", Css("display", "inline-block") . Css("width", "25%") . Css("vertical-align", "top")));
        $r2Div = HTML("div", $htmlContentR2, Attr("style", Css("display", "inline-block") . Css("width", "25%") . Css("vertical-align", "top")));

        return HTML("section", $lDiv . $l2Div . $rDiv . $r2Div);
    }
}

RegisterType('section/4-way', new FourWaySectionBuilder());