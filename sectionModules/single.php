<?php

class SingleSectionBuilder extends SectionBuilder
{
    protected function Build($data)
    {
        $type = $data->content->type;

        $htmlContent = GetBuilder("content/" . $type)->BuildHTML($data->content);

        return HTML("section", $htmlContent);
    }
}

RegisterType('section/single', new SingleSectionBuilder());