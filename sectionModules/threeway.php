<?php

class ThreeWaySectionBuilder extends SectionBuilder
{
    protected function Build($data)
    {
        $leftType = $data->leftContent->type;

        $htmlContentL = GetBuilder("content/" . $leftType)->BuildHTML($data->leftContent);
        
        $centerType = $data->centerContent->type;

        $htmlContentC = GetBuilder("content/" . $centerType)->BuildHTML($data->centerContent);

        $rightType = $data->rightContent->type;

        $htmlContentR = GetBuilder("content/" . $rightType)->BuildHTML($data->rightContent);

        $lDiv = HTML("div", $htmlContentL, Attr("style", Css("display", "inline-block") . Css("width", "33.33%") . Css("vertical-align", "top")));
        $cDiv = HTML("div", $htmlContentC, Attr("style", Css("display", "inline-block") . Css("width", "33.33%") . Css("vertical-align", "top")));
        $rDiv = HTML("div", $htmlContentR, Attr("style", Css("display", "inline-block") . Css("width", "33.33%") . Css("vertical-align", "top")));

        return HTML("section", $lDiv . $cDiv . $rDiv);
    }
}

RegisterType('section/3-way', new ThreeWaySectionBuilder());