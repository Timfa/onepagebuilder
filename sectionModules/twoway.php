<?php

class TwoWaySectionBuilder extends SectionBuilder
{
    protected function Build($data)
    {
        $leftType = $data->leftContent->type;

        $htmlContentL = GetBuilder("content/" . $leftType)->BuildHTML($data->leftContent);

        $rightType = $data->rightContent->type;

        $htmlContentR = GetBuilder("content/" . $rightType)->BuildHTML($data->rightContent);

        $lDiv = HTML("div", $htmlContentL, Attr("style", Css("display", "inline-block") . Css("width", "50%") . Css("vertical-align", "top")));
        $rDiv = HTML("div", $htmlContentR, Attr("style", Css("display", "inline-block") . Css("width", "50%") . Css("vertical-align", "top")));

        return HTML("section", $lDiv . $rDiv);
    }
}

RegisterType('section/2-way', new TwoWaySectionBuilder());